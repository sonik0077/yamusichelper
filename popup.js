(function () {

    function DownloadsViewBuilder(container) {
        var self = {
            container: container
        };

        this.buildDownloads = function (downloads) {
            var container = self.container;
            while (container.firstChild) {
                container.removeChild(container.firstChild);
            }
            if (downloads && downloads.length) {
                downloads.forEach(function (item) {
                    var download = document.createElement('div');
                    var span = document.createElement('span');
                    download.classList.add('progress-bar', 'blue', 'stripes');
                    span.style.width = item.progress + '%';
                    span.id = item.id;
                    span.textContent = item.title;
                    download.appendChild(span);
                    container.appendChild(download);
                });
            }
        }

    }

    function PopupController() {
        var self = {
            downloadsBuilder: null,
            downloadButton: document.querySelector('#track-link'),
            noTrackNote: document.querySelector('.no-track'),
            trackTitle: document.querySelector('#track-title'),
            trackInfo: document.querySelector('.track-info')
        };
        var that = this;

        self.init = function () {
            self.downloadButton = document.querySelector('#track-link');

            self.downloadsBuilder = new DownloadsViewBuilder(document.querySelector('.downloads'));
            self.notifyBackground();
        };

        self.hide = function (el) {
            if (el && el.style) {
                el.style.display = 'none';
            }
        };

        self.show = function (el) {
            if (el && el.style) {
                el.style.display = null;
            }
        };

        self.notifyBackground = function () {
            chrome.runtime.sendMessage({action: "popupOpen"});
        };

        this.updateDownload = function (download) {
            if (download) {
                var span = document.getElementById(download.id);
                if (span) {
                    //span.textContent = download.progress + '%';
                    span.style.width = download.progress + '%';
                }
            }
        };

        this.updateDownloads = function (downloads) {
            self.downloadsBuilder.buildDownloads(downloads);
        };

        this.onDownloadClick = function (callback) {
            self.onDownloadCallback = callback;
        };

        this.updateCurrentTrackView = function (data) {
            if (!data) {
                return;
            }
            var title = data.artists[0].title + ' - ' + data.title;
            self.hide(self.noTrackNote);
            self.show(self.trackInfo);
            self.trackTitle.textContent = title;
            if (data.downloading) {
                self.hide(self.downloadButton);
            } else {
                self.show(self.downloadButton);
                self.downloadButton.addEventListener('click', function () {
                    self.onDownloadCallback(data);
                });
            }
        };

        self.init();
    }

    window.onload = function () {
        window['popupController'] = new PopupController();
    };

})();