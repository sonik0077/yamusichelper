window.externalAPI.on(externalAPI.EVENT_TRACK, function () {
    var data = window.externalAPI.getCurrentTrack();
    var event = new CustomEvent('extensionDataResponse', {
        detail: {
            data: data
        }
    });
    document.dispatchEvent(event);
    });
