(function () {

    function PopupCommunicator(popupObjectName) {

        var self = {
            popupObjectName: popupObjectName,
            popupObject: null,
            onPopupOpen: null
        };

        self.init = function () {
            chrome.runtime.onMessage.addListener(function (request) {
                if (request['action'] == 'popupOpen') {
                    self.popupObject = self.getPopupGlobalObject(self.popupObjectName);
                    if (self.onPopupOpen && typeof self.onPopupOpen == 'function') {
                        self.onPopupOpen(request);
                    }
                }
            });
        };

        self.getPopupGlobalObject = function (objectName) {
            var popupWindows = chrome.extension.getViews({type: "popup"});
            if (popupWindows && popupWindows.length) {
                return popupWindows[0][objectName];
            }
        };

        this.getPopup = function () {
            return self.popupObject;
        };

        this.isOpen = function () {
            var res = chrome.extension.getViews({type: "popup"});
            return !!res.length;
        };

        this.onPopupOpen = function (callback) {
            self.onPopupOpen = callback;
        };

        self.init();
    }

    function ContentCommunicator(trackChangedListener) {

        this.init = function () {
            chrome.runtime.onMessage.addListener(listener);
        };

        var listener = function (request) {
            if (request && request['action'] == 'trackInfo') {
                console.log('Track info: ', request);
                this.currentTrackInfo = request.data;
                if (trackChangedListener) {
                    trackChangedListener(request.data);
                }
            }
        };

        this.init();
    }

    function MediaRequestsHandler(urlPattern, callback) {

        var that = this;
        var self = {
            urlPattern: urlPattern,
            callback: callback
        };

        self.init = function () {
            that.listen();
        };

        this.listen = function () {
            chrome.webRequest.onBeforeRequest.addListener(function (details) {
                that.currentMediaRequest = details;
                if (self.callback) {
                    self.callback(details);
                }
            }, {urls: [self.urlPattern]})
        };

        that.setCallback = function (callback) {
            self.callback = callback;
        };

        self.init();
    }

    function DownloadManager() {
        var that = this;

        var self = {
            counter: 0
        };

        this.downloads = [];

        self.getBytesFromUrl = function (url, download, handler, progressHandler) {

            that.downloads.push(download);
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);
            xhr.responseType = 'arraybuffer';
            xhr.onload = function () {
                that.downloads.splice(that.downloads.indexOf(download), 1);
                if (xhr.status === 200) {
                    var arrayBuffer = xhr.response;
                    handler(arrayBuffer);
                } else {
                    // handle error
                    console.error(xhr.statusText + ' (' + xhr.status + ')');
                }
            };
            xhr.onerror = function () {
                // handle error
                console.error('Network error');
                that.downloads.splice(that.downloads.indexOf(download), 1);
            };
            var loadPercent = 0;
            xhr.onprogress = function (event) {
                var currentPercent = ~~(event.loaded * 100 / event.total);
                if (currentPercent !== loadPercent) {
                    loadPercent = currentPercent;
                    download.progress = loadPercent;
                    if (progressHandler) {
                        progressHandler(loadPercent, download);
                    }
                }
            };
            xhr.send();
            return xhr;
        };

        this.isDownloading = function (url) {
            var result = false;
            if (that.downloads && url) {
                that.downloads.forEach(function (item) {
                    if (item.url === url) {
                        result = true;
                    }
                });
            }
            return result;
        };

        this.startDownload = function (url, finishCallback, progressCallback, title) {
            self.counter++;
            var download = {
                id: self.counter,
                url: url,
                title: title,
                progress: 0
            };
            self.getBytesFromUrl(url, download, finishCallback, progressCallback);
        };
    }

    function FileManager() {

        var self = {
            imageManager: null
        };

        self.init = function () {
            self.imageManager = new DownloadManager();
        };

        self.downloadImage = function (url, callback) {
            self.imageManager.startDownload(url, function (byteArray) {
                callback(byteArray);
            });
        };

        self.getFileTitle = function (trackInfo) {
            return trackInfo.artists[0].title + ' - ' + trackInfo.title + '.mp3';
        };

        self.setFileImage = function (writer, image) {
            writer.setFrame('APIC', image);
            writer.addTag();
        };

        self.getCoverImageUrl = function (url) {
            var params = {
                width: 300,
                height: 300
            };
            return 'https://' + url.replace('%%', params.width + 'x' + params.height);
        };

        self.setFileMetadata = function (arrayBuffer, metadata) {
            var writer = new ID3Writer(arrayBuffer);
            writer.setFrame('TIT2', metadata.title)
                .setFrame('TPE1', [metadata.artists.map(function (item) {
                    return item.title;
                })])
                .setFrame('TPE2', [metadata.artists[0].title])
                .setFrame('TYER', metadata.album.year)
                .setFrame('TALB', metadata.album.title);
            writer.addTag();
            return writer;
        };

        self.addToDownloads = function (url, filename) {
            chrome.downloads.download({
                url: url,
                filename: filename
            });
        };

        this.saveTrack = function (trackInfo, byteArray) {

            var writer = self.setFileMetadata(byteArray, trackInfo);
            if (trackInfo.cover) {
                self.downloadImage(self.getCoverImageUrl(trackInfo.cover), function (data) {
                    self.setFileImage(writer, data);
                    self.addToDownloads(writer.getURL(), self.getFileTitle(trackInfo));
                });
            } else {
                self.addToDownloads(writer.getURL(), self.getFileTitle(trackInfo));
            }
        };

        self.init();
    }

    var TrackInfoUtil = {
        getTrackIdFromUrl: function (url) {
            var regex = /(track-id=)([0-9]+)(&)/;
            var matches = regex.exec(url);
            if (matches.length > 3) {
                return matches[2];
            } else {
                return "";
            }
        }
    };

    function BackgroundProcessor() {

        var self = {
            currentTrackInfo: null
        };

        self.init = function () {
            self.downloadManager = new DownloadManager();
            self.mediaRequestHandler = new MediaRequestsHandler("*://*/get-mp3/*", self.onMediaRequest);
            self.popupCommunicator = new PopupCommunicator('popupController');
            self.contentCommunicator = new ContentCommunicator(self.trackChanged);
            self.popupCommunicator.onPopupOpen(self.onPopupOpen);
            self.fileManager = new FileManager();

        };

        self.trackChanged = function (data) {
            self.mediaRequestHandler.setCallback(function () {
                if (data) {
                    self.currentTrackInfo = data;
                    if (self.mediaRequestHandler.currentMediaRequest) {
                        self.currentTrackUrl = self.mediaRequestHandler.currentMediaRequest.url;
                    }
                    self.updateCurrentTrackView();
                }
                self.mediaRequestHandler.setCallback(undefined);
            });
        };

        self.downloadCurrentTrack = function () {

            if (!self.currentTrackUrl) {
                return;
            }

            var trackInfo = self.currentTrackInfo;
            var currentTrackUrl = self.currentTrackUrl;
            if (currentTrackUrl && !self.downloadManager.isDownloading(currentTrackUrl)) {
                self.mediaRequestHandler.ignoreRequests = true;
                self.downloadManager.startDownload(currentTrackUrl, function (bytesArray) {
                    self.fileManager.saveTrack(trackInfo, bytesArray);
                    self.updateCurrentTrackView();
                    self.updateDownloadsView();
                }, function (progress, currentDownload) {
                    console.log(progress);
                    if (self.popupCommunicator.isOpen()) {
                        self.popupCommunicator.getPopup().updateDownload(currentDownload);
                    }
                }, trackInfo.title);
                self.mediaRequestHandler.ignoreRequests = false;
                self.updateCurrentTrackView();
                self.updateDownloadsView();
            }

        };

        self.updateCurrentTrackView = function () {
            if (self.currentTrackInfo) {
                if (self.currentTrackUrl) {
                    self.currentTrackInfo.downloading = self.downloadManager.isDownloading(self.currentTrackUrl);
                }
                var controller = self.popupCommunicator.getPopup();
                if (controller) {
                    controller.updateCurrentTrackView(self.currentTrackInfo);
                }
            }
        };

        self.updateDownloadsView = function () {
            var controller = self.popupCommunicator.getPopup();
            controller.updateDownloads(self.downloadManager.downloads);
        };

        self.onPopupOpen = function () {
            self.updateCurrentTrackView();
            self.updateDownloadsView();
            self.popupCommunicator.getPopup().onDownloadClick(self.downloadCurrentTrack);
        };

        self.init();

    }

    window.context = new BackgroundProcessor();

})();