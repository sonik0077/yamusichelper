(function () {

    var injectScript = function () {
        var script = chrome.extension.getURL("injectScript.js");
        var element = document.createElement("script");
        element.src = script;
        document.querySelector('body').appendChild(element);
    };
    injectScript();

    document.addEventListener('extensionDataResponse', function (event) {
        chrome.runtime.sendMessage({action: 'trackInfo', data: event.detail.data});
    });
})();
